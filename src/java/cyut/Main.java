package java.cyut;

import java.util.*;
import java.lang.*;

public class Main {
    public static void main(String[] args) {
        Scanner sn = new Scanner(System.in);
        System.out.print("Please input a string:");
        try {
            String str = sn.next();
        }catch (Exception e){
            System.out.println("Wrong input!");
        }
        System.out.print("Reversed string:");
        doReverse(str);
    }
    public static void doReverse(String reverse) {
        for(int i = reverse.length() - 1;i >= 0;i--)
            System.out.print(reverse.charAt(i));
    }
}